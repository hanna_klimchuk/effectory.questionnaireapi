﻿ALTER TABLE [dbo].[TextTranslation]
DROP CONSTRAINT [FK_TextTranslation_Answer]
GO

ALTER TABLE [dbo].[TextTranslation]  WITH CHECK ADD  CONSTRAINT [FK_TextTranslation_Answer] FOREIGN KEY([AnswerId])
REFERENCES [dbo].[Answer] ([Id])
GO