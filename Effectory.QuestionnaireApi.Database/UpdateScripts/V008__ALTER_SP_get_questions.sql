﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_get_questions]
(
	@pageNumber int,
	@pageSize int,
	@subjectId int, 
	@culture nvarchar(10)
)
AS
BEGIN

IF(@pageNumber IS NULL 
	OR @pageSize IS NULL 
	OR @subjectId IS NULL
	OR @culture IS NULL)   
	RETURN;


;WITH Questions_Sorted AS (
	SELECT q.[Id], q.[OrderNumber], q.[SubjectId], q.[AnswerCategoryType],
		   t.[Culture], t.[Text], 
		   a.[Id] AS AnswerId, a.[OrderNumber] AS AnswerOrderNumber, a.[AnswerType],
		   DENSE_RANK() OVER (ORDER BY q.[Id], a.[Id] ASC) AS Rank_Sort
	FROM [dbo].[Question] AS q
	INNER JOIN [dbo].[Answer] AS a ON q.[Id] = a.[QuestionId]
	INNER JOIN [dbo].[TextTranslation] AS t ON q.[Id] = t.[QuestionId]
	WHERE q.[SubjectId] = @subjectId AND t.[Culture] = @culture
)
, AnswerText AS (
	SELECT [AnswerId], [Culture], [Text]
	FROM [dbo].[TextTranslation] AS t
	WHERE t.SubjectId = @subjectId AND t.[Culture] = @culture
)

SELECT q.[Id], q.[OrderNumber], q.[SubjectId], q.[AnswerCategoryType],
       q.[Culture], q.[Text],
	   q.[AnswerId] AS Id, q.[AnswerOrderNumber] AS OrderNumber, q.[AnswerType],
	   at.[Culture], at.[Text]
FROM Questions_Sorted AS q
INNER JOIN AnswerText AS at ON q.AnswerId = at.AnswerId
WHERE q.Rank_Sort BETWEEN ((@pageNumber - 1)*@pageSize + 1)
	AND ((@pageNumber - 1)*@pageSize + @pageSize)

END
GO