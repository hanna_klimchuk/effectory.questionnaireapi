﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_get_questions]
(
	@pageNumber int,
	@pageSize int,
	@subjectId int
)
AS
BEGIN

IF(@pageNumber IS NULL 
	OR @pageSize IS NULL 
	OR @subjectId IS NULL)   
	RETURN;


;WITH Questions_Sorted AS (
	SELECT q.[Id], q.[OrderNumber], q.[SubjectId], q.[AnswerCategoryType],
		   a.[Id] AS AnswerId, a.[OrderNumber] AS AnswerOrderNumber, a.[AnswerType], a.[Department], a.[UserId],
		   DENSE_RANK() OVER (ORDER BY q.[Id], a.[Id] ASC) AS Rank_Sort
	FROM [dbo].[Question] AS q
	INNER JOIN [dbo].[Answer] AS a ON q.[Id] = a.[QuestionId]
	WHERE [SubjectId] = @subjectId)
, Total_Count AS (
	SELECT MAX(Rank_Sort) AS TotalCount
	FROM Questions_Sorted
)
SELECT q.[Id], q.[OrderNumber], q.[SubjectId], q.[AnswerCategoryType],
	   q.[AnswerId], q.[AnswerOrderNumber], q.[AnswerType], q.[Department], q.[UserId],
	   t.TotalCount
FROM Questions_Sorted AS q, Total_Count AS t
WHERE q.Rank_Sort BETWEEN ((@pageNumber - 1)*@pageSize + 1)
	AND ((@pageNumber - 1)*@pageSize + @pageSize)

END
GO
