﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[sp_submit_answers]
GO

DROP TYPE [dbo].[udt_Answer]
GO

CREATE TYPE [dbo].[udt_UserAnswer] AS TABLE(
	[Department] [smallint] NOT NULL,
	[UserId] [int] NOT NULL,
	[AnswerId] [int] NOT NULL
)
GO

CREATE PROCEDURE [dbo].[sp_submit_answers]
	@userAnswers [udt_UserAnswer] READONLY
AS
BEGIN
	INSERT INTO [dbo].[UserAnswer]
           ([UserId]
           ,[Department]
           ,[AnswerId])
	SELECT [UserId]
           ,[Department]
           ,[AnswerId]
	FROM @userAnswers	
END
GO