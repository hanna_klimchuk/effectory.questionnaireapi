﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_get_questions]
(
	@pageNumber int,
	@pageSize int,
	@subjectId int, 
	@culture nvarchar(10)
)
AS
BEGIN

IF(@pageNumber IS NULL 
	OR @pageSize IS NULL 
	OR @subjectId IS NULL
	OR @culture IS NULL)   
	RETURN;


;WITH Questions_Sorted AS (
	SELECT q.[Id], q.[OrderNumber], q.[SubjectId], q.[AnswerCategoryType],
		   t.[Culture], t.[Text], 
		   a.[Id] AS AnswerId, a.[OrderNumber] AS AnswerOrderNumber, a.[AnswerType], a.[Department], a.[UserId],
		   DENSE_RANK() OVER (ORDER BY q.[Id], a.[Id] ASC) AS Rank_Sort
	FROM [dbo].[Question] AS q
	INNER JOIN [dbo].[Answer] AS a ON q.[Id] = a.[QuestionId]
	INNER JOIN [dbo].[TextTranslation] AS t ON q.[Id] = t.[QuestionId]
	WHERE q.[SubjectId] = @subjectId AND t.[Culture] = @culture)

SELECT q.[Id], q.[OrderNumber], q.[SubjectId], q.[AnswerCategoryType],
       q.[Culture], q.[Text],
	   q.[AnswerId], q.[AnswerOrderNumber], q.[AnswerType], q.[Department], q.[UserId]
FROM Questions_Sorted AS q
WHERE q.Rank_Sort BETWEEN ((@pageNumber - 1)*@pageSize + 1)
	AND ((@pageNumber - 1)*@pageSize + @pageSize)

END
GO