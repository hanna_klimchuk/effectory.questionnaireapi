﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TYPE [dbo].[udt_Answer] AS TABLE(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderNumber] [smallint] NOT NULL,
	[QuestionId] [int] NOT NULL,
	[AnswerType] [smallint] NOT NULL,
	[Department] [smallint] NOT NULL,
	[UserId] [int] NOT NULL
)
GO

CREATE PROCEDURE [dbo].[sp_submit_answers]
	@answers [udt_Answer] READONLY
AS
BEGIN
	INSERT INTO [dbo].[Answer]
           ([OrderNumber]
           ,[QuestionId]
           ,[AnswerType]
           ,[Department]
           ,[UserId])
	SELECT [OrderNumber]
           ,[QuestionId]
           ,[AnswerType]
           ,[Department]
           ,[UserId]
	FROM @answers	
END
GO