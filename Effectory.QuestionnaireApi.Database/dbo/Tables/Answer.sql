﻿CREATE TABLE [dbo].[Answer] (
    [Id]          INT      IDENTITY (1, 1) NOT NULL,
    [OrderNumber] SMALLINT NOT NULL,
    [QuestionId]  INT      NOT NULL,
    [AnswerType]  SMALLINT NOT NULL,
    CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Answer_Question] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id])
);



