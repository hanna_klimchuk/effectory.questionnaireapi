﻿CREATE TABLE [dbo].[schema_version] (
    [installed_rank] INT             NOT NULL,
    [version]        NVARCHAR (50)   NULL,
    [description]    NVARCHAR (200)  NULL,
    [type]           NVARCHAR (20)   NOT NULL,
    [script]         NVARCHAR (1000) NOT NULL,
    [checksum]       INT             NULL,
    [installed_by]   NVARCHAR (100)  NOT NULL,
    [installed_on]   DATETIME        DEFAULT (getdate()) NOT NULL,
    [execution_time] INT             NOT NULL,
    [success]        BIT             NOT NULL,
    CONSTRAINT [schema_version_pk] PRIMARY KEY CLUSTERED ([installed_rank] ASC)
);


GO
CREATE NONCLUSTERED INDEX [schema_version_s_idx]
    ON [dbo].[schema_version]([success] ASC);

