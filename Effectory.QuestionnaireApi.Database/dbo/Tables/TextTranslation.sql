﻿CREATE TABLE [dbo].[TextTranslation] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [SubjectId]  INT            NOT NULL,
    [QuestionId] INT            NULL,
    [AnswerId]   INT            NULL,
    [Culture]    NVARCHAR (10)  NOT NULL,
    [Text]       NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_TextTranslation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TextTranslation_Answer] FOREIGN KEY ([AnswerId]) REFERENCES [dbo].[Answer] ([Id]),
    CONSTRAINT [FK_TextTranslation_Question] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id]),
    CONSTRAINT [FK_TextTranslation_Subject] FOREIGN KEY ([SubjectId]) REFERENCES [dbo].[Subject] ([Id])
);



