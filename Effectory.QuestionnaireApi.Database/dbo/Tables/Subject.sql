﻿CREATE TABLE [dbo].[Subject] (
    [Id]          INT      IDENTITY (1, 1) NOT NULL,
    [OrderNumber] SMALLINT NOT NULL,
    CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED ([Id] ASC)
);

