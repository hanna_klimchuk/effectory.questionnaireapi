﻿CREATE TABLE [dbo].[UserAnswer] (
    [Id]         INT      IDENTITY (1, 1) NOT NULL,
    [UserId]     INT      NOT NULL,
    [Department] SMALLINT NOT NULL,
    [AnswerId]   INT      NOT NULL,
    CONSTRAINT [PK_UserAnswer] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserAnswer_Answer] FOREIGN KEY ([AnswerId]) REFERENCES [dbo].[Answer] ([Id])
);

