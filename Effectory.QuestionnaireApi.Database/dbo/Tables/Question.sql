﻿CREATE TABLE [dbo].[Question] (
    [Id]                 INT      IDENTITY (1, 1) NOT NULL,
    [OrderNumber]        SMALLINT NOT NULL,
    [SubjectId]          INT      NOT NULL,
    [AnswerCategoryType] SMALLINT NOT NULL,
    CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Question_Subject] FOREIGN KEY ([SubjectId]) REFERENCES [dbo].[Subject] ([Id])
);

