﻿CREATE PROCEDURE [dbo].[sp_submit_answers]
	@userAnswers [udt_UserAnswer] READONLY
AS
BEGIN
	INSERT INTO [dbo].[UserAnswer]
           ([UserId]
           ,[Department]
           ,[AnswerId])
	SELECT [UserId]
           ,[Department]
           ,[AnswerId]
	FROM @userAnswers	
END