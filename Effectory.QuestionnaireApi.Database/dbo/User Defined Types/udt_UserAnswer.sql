﻿CREATE TYPE [dbo].[udt_UserAnswer] AS TABLE (
    [Department] SMALLINT NOT NULL,
    [UserId]     INT      NOT NULL,
    [AnswerId]   INT      NOT NULL);

