﻿using System.Threading.Tasks;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;

namespace Effectory.QuestionnaireApi.Core.Interfaces
{
    public interface IUserAnswersService
    {
        Task SaveAll(UserAnswerRequest[] userAnswers);
        Task<UserAnswerStatisticOverview> GetStatisticAcrossDepartments(int answerId);
    }
}
