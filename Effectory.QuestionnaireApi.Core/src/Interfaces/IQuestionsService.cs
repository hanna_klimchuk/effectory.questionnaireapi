﻿using System.Threading.Tasks;
using Effectory.QuestionnaireApi.Core.Models;

namespace Effectory.QuestionnaireApi.Core.Interfaces
{
    public interface IQuestionsService
    {
        Task<QuestionOverview[]> Get(int subjectId, PageInfo pageInfo);
    }
}
