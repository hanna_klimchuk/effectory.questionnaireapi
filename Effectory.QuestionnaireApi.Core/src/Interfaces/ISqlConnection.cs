﻿using System.Data;

namespace Effectory.QuestionnaireApi.Core.Interfaces
{
    public interface ISqlConnection
    {
        IDbConnection GetConnection();
    }
}
