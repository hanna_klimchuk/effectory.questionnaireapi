﻿namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class UserAnswerRequest
    {
        public short Department { get; set; }
        public int UserId { get; set; }
        public int AnswerId { get; set; }
    }
}
