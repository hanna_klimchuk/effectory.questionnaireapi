﻿namespace Effectory.QuestionnaireApi.Core.Models
{
    public class PageInfo
    {
        public int Size { get; set; } = 20;
        public int Number { get; set; } = 1;
        public string Culture { get; set; } = "en-US";
    }
}
