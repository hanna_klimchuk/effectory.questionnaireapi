﻿namespace Effectory.QuestionnaireApi.Core.Models
{
    public class UserAnswerStatisticOverview
    {
        public int AnswerId { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int Average { get; set; }
    }
}
