﻿namespace Effectory.QuestionnaireApi.Core.Models
{
    public class TextOverview
    {
        public string Culture { get; set; }
        public string Text { get; set; }
    }
}
