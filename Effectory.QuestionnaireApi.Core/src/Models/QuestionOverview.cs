﻿namespace Effectory.QuestionnaireApi.Core.Models
{
    public class QuestionOverview
    {
        public short OrderNumber { get; set; }
        public int SubjectId { get; set; }
        public short AnswerCategoryType { get; set; }
        public TextOverview[] Texts { get; set; }
        public AnswerOverview[] Answers { get; set; }
    }
}
