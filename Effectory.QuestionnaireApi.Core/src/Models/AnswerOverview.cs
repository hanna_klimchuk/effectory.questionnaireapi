﻿namespace Effectory.QuestionnaireApi.Core.Models
{
    public class AnswerOverview
    {
        public int QuestionId { get; set; }
        public short AnswerType { get; set; }
        public short OrderNumber { get; set; }
        public TextOverview[] Texts { get; set; }
    }
}
