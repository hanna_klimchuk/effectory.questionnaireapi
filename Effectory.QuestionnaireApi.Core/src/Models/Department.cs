﻿namespace Effectory.QuestionnaireApi.Core.Models
{
    public enum Department
    {
        Marketing = 0, 
        Sales, 
        Development, 
        Reception
    }
}
