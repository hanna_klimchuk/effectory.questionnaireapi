﻿namespace Effectory.QuestionnaireApi.Core.Options
{
    public class QuestionnaireOptions
    {
        public const string Questionnaire = "Questionnaire";
        public string ConnectionString { get; set; }
    }
}
