﻿namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class TextTranslation
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public int? QuestionId { get; set; }
        public int? AnswerId { get; set; }
        public string Culture { get; set; }
        public string Text { get; set; }
    }
}
