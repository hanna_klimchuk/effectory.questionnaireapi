﻿namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class UserAnswer
    {
        public int Id { get; set; }
        public short Department { get; set; }
        public int UserId { get; set; }
        public int AnswerId { get; set; }
    }
}
