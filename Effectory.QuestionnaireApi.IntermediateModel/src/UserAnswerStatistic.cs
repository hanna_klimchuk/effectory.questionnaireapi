﻿namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class UserAnswerStatistic
    {
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int Average { get; set; }
    }
}
