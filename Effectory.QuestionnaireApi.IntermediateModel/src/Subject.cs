﻿using System.Collections.Generic;

namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class Subject : QuestionnaireItem
    {
        public IEnumerable<Question> Questions { get; set; }
    }
}
