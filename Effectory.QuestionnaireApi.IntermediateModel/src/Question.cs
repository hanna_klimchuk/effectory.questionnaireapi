﻿using System.Collections.Generic;

namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class Question : QuestionnaireItem
    {
        public int SubjectId { get; set; }
        public short AnswerCategoryType { get; set; }
        public IEnumerable<Answer> Answers { get; set; }
    }
}
