﻿using System.Collections.Generic;

namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public abstract class QuestionnaireItem
    {
        public int Id { get; set; }
        public short OrderNumber { get; set; }
        public IEnumerable<TextTranslation> Texts { get; set; }
    }
}
