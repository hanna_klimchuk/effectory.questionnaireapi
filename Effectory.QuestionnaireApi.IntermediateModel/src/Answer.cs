﻿namespace Effectory.QuestionnaireApi.IntermediateModel
{
    public class Answer : QuestionnaireItem
    {
        public int QuestionId { get; set; }
        public short AnswerType { get; set; }
    }
}
