using System.Threading.Tasks;
using AutoMapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;
using Effectory.QuestionnaireApi.Services.Mapping;
using Effectory.QuestionnaireApi.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace Effectory.QuestionnaireApi.Web.Test
{
    public class UserAnswersControllerTest
    {
        private Mock<IUserAnswersService> _userAnswersServiceMock;
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _userAnswersServiceMock = new Mock<IUserAnswersService>();
        }

        [Test]
        public async Task UserAnswersController_Save_Verify()
        {
            var userAnswers = new UserAnswerRequest[]
            {
                new UserAnswerRequest() {AnswerId = 1, Department = 0, UserId = 1}, 
                new UserAnswerRequest() {AnswerId = 2, Department = 1, UserId = 1},
                new UserAnswerRequest() {AnswerId = 3, Department = 2, UserId = 1},
                new UserAnswerRequest() {AnswerId = 1, Department = 3, UserId = 2},
                new UserAnswerRequest() {AnswerId = 3, Department = 2, UserId = 2},
                new UserAnswerRequest() {AnswerId = 5, Department = 0, UserId = 3}
            };
            _userAnswersServiceMock
                .Setup(s => s.SaveAll(userAnswers));

            var controller = new UserAnswersController(_userAnswersServiceMock.Object);

            await controller.Save(userAnswers);

            _userAnswersServiceMock.Verify(x => x.SaveAll(userAnswers), Times.Once);
        }

        [Test]
        public async Task UserAnswersController_GetStatistic_Verify()
        {
            _userAnswersServiceMock
                .Setup(s => s.GetStatisticAcrossDepartments(It.IsAny<int>()))
                .ReturnsAsync(new UserAnswerStatisticOverview
                {
                    AnswerId = 1,
                    Average = 2,
                    Maximum = 4,
                    Minimum = 1
                });

            var controller = new UserAnswersController(_userAnswersServiceMock.Object);

            await controller.GetStatistic(1);
            
            _userAnswersServiceMock.Verify(x => x.GetStatisticAcrossDepartments(1), Times.Once);
        }
    }
}