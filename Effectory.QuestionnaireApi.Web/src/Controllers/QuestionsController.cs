﻿using System;
using System.Threading.Tasks;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Effectory.QuestionnaireApi.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuestionsController : ControllerBase
    {
        private readonly IQuestionsService _questionsService;

        public QuestionsController(IQuestionsService questionsService)
        {
            _questionsService = questionsService;
        }

        [HttpGet]
        [Route("{subjectId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get([FromRoute] int subjectId, [FromQuery] PageInfo pageInfo)
        {
            return Ok(await _questionsService.Get(subjectId, pageInfo));
        }
    }
}
