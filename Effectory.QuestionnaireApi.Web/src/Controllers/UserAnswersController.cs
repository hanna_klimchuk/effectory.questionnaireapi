﻿using System.Threading.Tasks;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.IntermediateModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Effectory.QuestionnaireApi.Web.Controllers
{
    [ApiController]
    [Route("api/user-answers")]
    public class UserAnswersController : ControllerBase
    {
        private readonly IUserAnswersService _userAnswersService;

        public UserAnswersController(IUserAnswersService userAnswersService)
        {
            _userAnswersService = userAnswersService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Save([FromBody] UserAnswerRequest[] userAnswers)
        {
            await _userAnswersService.SaveAll(userAnswers);
            return StatusCode(201);
        }

        [HttpGet]
        [Route("{answerId}/statistic")]
        public async Task<IActionResult> GetStatistic([FromRoute]int answerId)
        {
            return Ok(await _userAnswersService.GetStatisticAcrossDepartments(answerId));
        }
    }
}
