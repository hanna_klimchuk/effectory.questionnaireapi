using AutoMapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Options;
using Effectory.QuestionnaireApi.Repository;
using Effectory.QuestionnaireApi.Repository.Base;
using Effectory.QuestionnaireApi.Repository.Interfaces;
using Effectory.QuestionnaireApi.Services;
using Effectory.QuestionnaireApi.Services.Mapping;
using Effectory.QuestionnaireApi.Web.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace Effectory.QuestionnaireApi.Web
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                    builder.SetIsOriginAllowed(_ => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            var questionnaireOptions = new QuestionnaireOptions();
            _configuration.GetSection(QuestionnaireOptions.Questionnaire).Bind(questionnaireOptions);
            services.AddSingleton(questionnaireOptions);

            services.AddTransient<IQuestionnaireConnection, QuestionnaireConnection>();

            services.AddScoped<IQuestionsRepository, QuestionsRepository>();
            services.AddScoped<IUserAnswersRepository, UserAnswersRepository>();

            services.AddScoped<IQuestionsService, QuestionsService>();
            services.AddScoped<IUserAnswersService, UserAnswersService>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services
                .AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseRouting();
            app.UseCors();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
