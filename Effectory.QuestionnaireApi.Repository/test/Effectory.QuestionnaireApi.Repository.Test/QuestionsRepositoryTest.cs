using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.Repository.Interfaces;
using Effectory.QuestionnaireApi.Repository.Test.Base;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Effectory.QuestionnaireApi.Repository.Test
{
    [TestFixture]
    public class QuestionsRepositoryTest : BaseQuestionnaireRepositoryTest
    {
        [Test]
        public async Task QuestionsRepository_GetAll()
        {
            // Arrange
            var subjectId = CreateSubject();
            CreateData(subjectId);

            var questionsRepository = _serviceProvider.GetService<IQuestionsRepository>();

            // Act
            var result = await questionsRepository.GetBy(subjectId, new PageInfo());

            // Assert
            Assert.NotNull(result);
            Assert.IsTrue(result.Length == 1);
            Assert.IsTrue(result[0].Texts.Count() == 1);
            Assert.IsTrue(result[0].Answers.Count() == 5);
        }

        [Test]
        public async Task QuestionsRepository_GetAll_PageInfo()
        {
            // Arrange
            var subjectId = CreateSubject();
            CreateData(subjectId);

            var questionsRepository = _serviceProvider.GetService<IQuestionsRepository>();

            // Act
            var result = await questionsRepository.GetBy(subjectId, new PageInfo() { Number = 3, Size = 2 });

            // Assert
            Assert.NotNull(result);
            Assert.IsTrue(result.Length == 1);
            Assert.IsTrue(result[0].Texts.Count() == 1);
            Assert.IsTrue(result[0].Answers.Count() == 1);
        }


        private int CreateSubject()
        {
            var connection = _serviceProvider.GetService<IQuestionnaireConnection>();
            using var con = connection.GetConnection();

            var subjectId = con.ExecuteScalar<int>("INSERT [dbo].[Subject] ([OrderNumber]) OUTPUT INSERTED.Id VALUES (0)");

            return subjectId;
        }

        private void CreateData(int subjectId)
        {
            var bulkInsert = @"
DECLARE @questionId int;
INSERT [dbo].[Question] ([OrderNumber], [SubjectId], [AnswerCategoryType]) VALUES (0, @subjectId, 0)
SET @questionId = (SELECT SCOPE_IDENTITY()); 

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, NULL, NULL, N'nl-NL', N'Mijn werk')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, NULL, NULL, N'en-US', N'My work')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, NULL, N'nl-NL', N'Ik ben blij met mijn werk')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, NULL, N'en-US', N'I am happy with my work')

INSERT [dbo].[Question] ([OrderNumber], [SubjectId], [AnswerCategoryType]) VALUES (1, @subjectId, 0)
SET @questionId = (SELECT SCOPE_IDENTITY()); 

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, NULL, N'en-US', N'I enjoy doing my work')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, NULL, N'nl-NL', N'Ik doe mijn werk met plezier')

DECLARE @answerId int;
INSERT [dbo].[Answer] ([OrderNumber], [QuestionId], [AnswerType]) VALUES (0, @questionId, 1)
SET @answerId = (SELECT SCOPE_IDENTITY());

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'en-US', N'Strongly disagree')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'nl-NL', N'Helemaal mee oneens')

INSERT [dbo].[Answer] ([OrderNumber], [QuestionId], [AnswerType]) VALUES (1, @questionId, 1)
SET @answerId = (SELECT SCOPE_IDENTITY());

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'nl-NL', N'Mee oneens')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'en-US', N'Disagree')


INSERT [dbo].[Answer] ([OrderNumber], [QuestionId], [AnswerType]) VALUES (2, @questionId, 1)
SET @answerId = (SELECT SCOPE_IDENTITY());

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'nl-NL', N'Niet mee eens/ niet mee oneens')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'en-US', N'Neither agree nor disagree')

INSERT [dbo].[Answer] ([OrderNumber], [QuestionId], [AnswerType]) VALUES (3, @questionId, 1)
SET @answerId = (SELECT SCOPE_IDENTITY());

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'nl-NL', N'Mee eens')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'en-US', N'Agree')

INSERT [dbo].[Answer] ([OrderNumber], [QuestionId], [AnswerType]) VALUES (4, @questionId, 1)
SET @answerId = (SELECT SCOPE_IDENTITY());

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'nl-NL', N'Helemaal mee eens')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'en-US', N'Strongly agree')
";
            var connection = _serviceProvider.GetService<IQuestionnaireConnection>();
            using var con = connection.GetConnection();

            con.Execute(bulkInsert, new
            {
                subjectId = subjectId
            });
        }
    }
}