﻿using System.IO;
using System.Text;
using Dapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Options;
using Effectory.QuestionnaireApi.Repository.Base;
using Effectory.QuestionnaireApi.Repository.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Effectory.QuestionnaireApi.Repository.Test.Base
{
    public class BaseQuestionnaireRepositoryTest
    {
        internal IServiceCollection _services;
        internal ServiceProvider _serviceProvider;

        [SetUp]
        public virtual void SetUp()
        {
            _services = new ServiceCollection();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var questionnaireOptions = new QuestionnaireOptions();
            configuration.GetSection(QuestionnaireOptions.Questionnaire).Bind(questionnaireOptions);
            _services.AddSingleton(questionnaireOptions);
            _services.AddSingleton(configuration);

            _services.AddTransient<IQuestionnaireConnection, QuestionnaireConnection>();
            _services.AddSingleton<IQuestionsRepository, QuestionsRepository>();
            _services.AddSingleton<IUserAnswersRepository, UserAnswersRepository>();

            _serviceProvider = _services.BuildServiceProvider();
        }

        [TearDown]

        public void TearDown()
        {
            var adServiceConnection = _serviceProvider.GetService<IQuestionnaireConnection>();

            var query = new StringBuilder();
            query.AppendLine("DELETE FROM [QuestionnaireIntegraitionTests].[dbo].[TextTranslation]");
            query.AppendLine("DELETE FROM [QuestionnaireIntegraitionTests].[dbo].[UserAnswer]");
            query.AppendLine("DELETE FROM [QuestionnaireIntegraitionTests].[dbo].[Answer]");
            query.AppendLine("DELETE FROM [QuestionnaireIntegraitionTests].[dbo].[Question]");
            query.AppendLine("DELETE FROM [QuestionnaireIntegraitionTests].[dbo].[Subject]");

            using var connection = adServiceConnection?.GetConnection();
            connection.Execute(query.ToString());
        }
    }
}
