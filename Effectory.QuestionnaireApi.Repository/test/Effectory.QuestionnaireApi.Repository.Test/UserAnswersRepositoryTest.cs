using System.Threading.Tasks;
using Dapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;
using Effectory.QuestionnaireApi.Repository.Interfaces;
using Effectory.QuestionnaireApi.Repository.Test.Base;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Effectory.QuestionnaireApi.Repository.Test
{
    [TestFixture]
    public class UserAnswersRepositoryTest : BaseQuestionnaireRepositoryTest
    {
        [Test]
        public async Task UserAnswersRepository_SaveAll_And_GetStatisticAcrossDepartments()
        {
            // Arrange
            var answerId = await CreateData();

            var userAnswers = new UserAnswer[]
            {
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Development, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Sales, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Reception, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Reception, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Development, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Reception, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Marketing, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Development, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Reception, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Development, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Sales, UserId = 3 },
                new UserAnswer() { AnswerId = answerId, Department = (short)Department.Reception, UserId = 3 },
            };

            var userAnswersRepository = _serviceProvider.GetService<IUserAnswersRepository>();

            // Act
            await userAnswersRepository.SaveAll(userAnswers);
            var statictic = await userAnswersRepository.GetStatisticAcrossDepartments(answerId);

            // Assert
            Assert.NotNull(statictic);
            Assert.AreEqual(5, statictic.Maximum);
            Assert.AreEqual(1, statictic.Minimum);
            Assert.AreEqual(3, statictic.Average);
        }

        private async Task<int> CreateData()
        {
            var bulkInsert = @"
DECLARE @subjectId int;
INSERT [dbo].[Subject] ([OrderNumber]) VALUES (0)
SET @subjectId = (SELECT SCOPE_IDENTITY()); 

DECLARE @questionId int;
INSERT [dbo].[Question] ([OrderNumber], [SubjectId], [AnswerCategoryType]) VALUES (0, @subjectId, 0)
SET @questionId = (SELECT SCOPE_IDENTITY()); 

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, NULL, NULL, N'nl-NL', N'Mijn werk')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, NULL, NULL, N'en-US', N'My work')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, NULL, N'nl-NL', N'Ik ben blij met mijn werk')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, NULL, N'en-US', N'I am happy with my work')

DECLARE @answerId int;
INSERT [dbo].[Answer] ([OrderNumber], [QuestionId], [AnswerType]) VALUES (0, @questionId, 1)
SET @answerId = (SELECT SCOPE_IDENTITY());

INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'en-US', N'Strongly disagree')
INSERT [dbo].[TextTranslation] ([SubjectId], [QuestionId], [AnswerId], [Culture], [Text]) VALUES (@subjectId, @questionId, @answerId, N'nl-NL', N'Helemaal mee oneens')

SELECT @answerId
";
            var connection = _serviceProvider.GetService<IQuestionnaireConnection>();
            using var con = connection.GetConnection();

            return await con.ExecuteScalarAsync<int>(bulkInsert);
        }
    }
}