﻿using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.IntermediateModel;
using Effectory.QuestionnaireApi.Repository.Base;
using Effectory.QuestionnaireApi.Repository.Interfaces;

namespace Effectory.QuestionnaireApi.Repository
{
    public class UserAnswersRepository : RepositoryBase, IUserAnswersRepository
    {
        public UserAnswersRepository(IQuestionnaireConnection connection) : base(connection)
        {
        }

        public async Task SaveAll(UserAnswer[] userAnswers)
        {
            if (userAnswers == null
                || !userAnswers.Any())
                return;

            var userAnswersDt = GenerateUserAnswersUdtDataTable();
            GenerateUserAnswersToSubmit(userAnswers, userAnswersDt);

            var parameters = new DynamicParameters();
            parameters.Add("@userAnswers", userAnswersDt.AsTableValuedParameter("udt_UserAnswer"));

            using var con = Connection.GetConnection();
            await con.ExecuteAsync("[dbo].[sp_submit_answers] @userAnswers", parameters);
        }

        public async Task<UserAnswerStatistic> GetStatisticAcrossDepartments(int answerId)
        {
            var query = new StringBuilder();
            query.AppendLine("SELECT MIN(AnswersCount) AS Minimum, MAX(AnswersCount) AS Maximum, AVG(AnswersCount) AS Average ");
            query.AppendLine("FROM (");
            query.AppendLine("SELECT ua.[Department], ua.[AnswerId], COUNT(*) as AnswersCount ");
            query.AppendLine("FROM [dbo].[UserAnswer] AS ua ");
            query.AppendLine("GROUP BY ua.[Department], ua.[AnswerId] ");
            query.AppendLine("HAVING ua.[AnswerId]=@answerId");
            query.AppendLine(") AS Result");

            var parameters = new DynamicParameters();
            parameters.Add("@answerId", answerId, null, ParameterDirection.Input);

            using var con = Connection.GetConnection();
            var statistic = await con.QuerySingleAsync<UserAnswerStatistic>(query.ToString(), parameters);

            return statistic;
        }

        private static void GenerateUserAnswersToSubmit(UserAnswer[] userAnswers, DataTable dataTable)
        {
            foreach (var userAnswer in userAnswers)
            {
                var userAnswerRow = ConvertUserAnswerToUdt(dataTable, userAnswer);
                dataTable.Rows.Add(userAnswerRow);
            }
        }

        private static DataRow ConvertUserAnswerToUdt(DataTable dataTable, UserAnswer userAnswer)
        {
            var userAnswerRow = dataTable.NewRow();

            userAnswerRow[0] = userAnswer.Department;
            userAnswerRow[1] = userAnswer.UserId;
            userAnswerRow[2] = userAnswer.AnswerId;

            return userAnswerRow;
        }

        private static DataTable GenerateUserAnswersUdtDataTable()
        {
            var userAnswersDt = new DataTable();

            userAnswersDt.Columns.Add("Department", typeof(short));
            userAnswersDt.Columns.Add("UserId", typeof(int));
            userAnswersDt.Columns.Add("AnswerId", typeof(int));

            return userAnswersDt;
        }
    }
}
