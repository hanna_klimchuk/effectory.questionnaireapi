﻿using System.Threading.Tasks;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;

namespace Effectory.QuestionnaireApi.Repository.Interfaces
{
    public interface IQuestionsRepository
    {
        Task<Question[]> GetBy(int subjectId, PageInfo pageInfo = null);
    }
}
