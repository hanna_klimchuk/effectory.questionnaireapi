﻿using System.Threading.Tasks;
using Effectory.QuestionnaireApi.IntermediateModel;

namespace Effectory.QuestionnaireApi.Repository.Interfaces
{
    public interface IUserAnswersRepository
    {
        Task SaveAll(UserAnswer[] userAnswers);
        Task<UserAnswerStatistic> GetStatisticAcrossDepartments(int answerId);
    }
}
