﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;
using Effectory.QuestionnaireApi.Repository.Base;
using Effectory.QuestionnaireApi.Repository.Interfaces;

namespace Effectory.QuestionnaireApi.Repository
{
    public class QuestionsRepository : RepositoryBase, IQuestionsRepository
    {
        public QuestionsRepository(IQuestionnaireConnection connection) : base(connection)
        {
        }

        public async Task<Question[]> GetBy(int subjectId, PageInfo pageInfo)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@pageNumber", pageInfo.Number, null, ParameterDirection.Input);
            parameters.Add("@pageSize", pageInfo.Size, null, ParameterDirection.Input);
            parameters.Add("@subjectId", subjectId, null, ParameterDirection.Input);
            parameters.Add("@culture", pageInfo.Culture, null, ParameterDirection.Input);

            using var con = Connection.GetConnection();
            var questionsRes = await con.QueryAsync<Question, TextTranslation, Answer, TextTranslation, Question>("[dbo].[sp_get_questions]",
                (question, text, answer, answerText) =>
                {
                    text.SubjectId = question.SubjectId;
                    text.QuestionId = question.Id;

                    answer.QuestionId = question.Id;

                    answerText.SubjectId = question.SubjectId;
                    answerText.QuestionId = question.Id;
                    answerText.AnswerId = answer.Id;

                    question.Texts = new TextTranslation[] { text };
                    question.Answers = new Answer[] { answer };
                    answer.Texts = new TextTranslation[] { answerText };
                    return question;
                },
                parameters,
                commandType: CommandType.StoredProcedure,
                splitOn: "Culture,Id,Culture");

            var questions = questionsRes
                .GroupBy(q => q.Id)
                .Select(g => new Question
                {
                    Id = g.Key,
                    AnswerCategoryType = g.First().AnswerCategoryType,
                    OrderNumber = g.First().OrderNumber,
                    SubjectId = g.First().SubjectId,
                    Texts = g.First().Texts,
                    Answers = g.SelectMany(q => q.Answers)
                        .GroupBy(a => a.Id)
                        .Select(g => new Answer
                        {
                            Id = g.Key,
                            AnswerType = g.First().AnswerType,
                            OrderNumber = g.First().OrderNumber,
                            QuestionId = g.First().QuestionId,
                            Texts = g.First().Texts
                        })
                        .ToArray()
                })
                .ToArray();

            return questions;
        }
    }
}
