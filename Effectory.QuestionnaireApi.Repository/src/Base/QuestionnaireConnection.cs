﻿using System.Data;
using System.Data.SqlClient;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Options;

namespace Effectory.QuestionnaireApi.Repository.Base
{
    public class QuestionnaireConnection : IQuestionnaireConnection
    {
        private SqlConnection connection;
        public readonly string _connectionString;

        public QuestionnaireConnection(QuestionnaireOptions options)
        {
            _connectionString = options.ConnectionString;
        }

        public IDbConnection GetConnection()
        {
            return connection ?? new SqlConnection(_connectionString);
        }
    }
}
