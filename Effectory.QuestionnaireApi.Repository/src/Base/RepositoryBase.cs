﻿using Effectory.QuestionnaireApi.Core.Interfaces;

namespace Effectory.QuestionnaireApi.Repository.Base
{
    public abstract class RepositoryBase
    {
        internal readonly IQuestionnaireConnection Connection;

        internal RepositoryBase(IQuestionnaireConnection connection)
        {
            Connection = connection;
        }
    }
}
