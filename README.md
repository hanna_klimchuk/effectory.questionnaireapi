# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an implementation of the test assignment available via the link https://github.com/Effectory/coding-assignment-api

### How do I get set up? ###

* Clone #develop# branch
* Create empty MSSQL Server database, for example Questionnaire
* Go to the path: effectory.questionnaireapi\Flyway and copy the script Hanna_migrate.sql
* Update the connectition details in your copy
* Run this script. It will run database migrations 
* Update the connection string in effectory.questionnaireapi\Effectory.QuestionnaireApi.Web\src\appsettings.Development.json
* Build and run the application

### Technologies ###

* .NET Core, Dapper, Automapper