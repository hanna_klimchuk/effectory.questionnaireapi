﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;
using Effectory.QuestionnaireApi.Repository.Interfaces;

namespace Effectory.QuestionnaireApi.Services
{
    public class UserAnswersService : IUserAnswersService
    {
        private readonly IUserAnswersRepository _userAnswersRepository;
        private readonly IMapper _mapper;

        public UserAnswersService(IUserAnswersRepository userAnswersRepository,
            IMapper mapper)
        {
            _userAnswersRepository = userAnswersRepository;
            _mapper = mapper;
        }

        public async Task SaveAll(UserAnswerRequest[] userAnswersRequest)
        {
            if (userAnswersRequest.Any())
            {
                var userAnswers = _mapper.Map<UserAnswer[]>(userAnswersRequest);
                await _userAnswersRepository.SaveAll(userAnswers);
            }
        }

        public async Task<UserAnswerStatisticOverview> GetStatisticAcrossDepartments(int answerId)
        {
            var statistic = await _userAnswersRepository.GetStatisticAcrossDepartments(answerId);

            var statisticOverview = _mapper.Map<UserAnswerStatisticOverview>(statistic);
            statisticOverview.AnswerId = answerId;

            return statisticOverview;
        }
    }
}
