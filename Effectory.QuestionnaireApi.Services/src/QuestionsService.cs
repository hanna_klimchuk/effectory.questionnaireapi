﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Effectory.QuestionnaireApi.Core.Interfaces;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.Repository.Interfaces;

namespace Effectory.QuestionnaireApi.Services
{
    public class QuestionsService : IQuestionsService
    {
        private readonly IQuestionsRepository _questionsRepository;
        private readonly IMapper _mapper;

        public QuestionsService(IQuestionsRepository questionsRepository,
            IMapper mapper)
        {
            _questionsRepository = questionsRepository;
            _mapper = mapper;
        }

        public async Task<QuestionOverview[]> Get(int subjectId, PageInfo pageInfo)
        {
            var questionsDb = await _questionsRepository.GetBy(subjectId, pageInfo);
            if (questionsDb == null)
                return new QuestionOverview[0];

            return _mapper.Map<QuestionOverview[]>(questionsDb);
        }
    }
}
