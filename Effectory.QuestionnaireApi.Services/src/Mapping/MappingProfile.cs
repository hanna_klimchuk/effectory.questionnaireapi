﻿using AutoMapper;
using Effectory.QuestionnaireApi.Core.Models;
using Effectory.QuestionnaireApi.IntermediateModel;

namespace Effectory.QuestionnaireApi.Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TextTranslation, TextOverview>();
            CreateMap<Answer, AnswerOverview>();
            CreateMap<UserAnswerStatistic, UserAnswerStatisticOverview>()
                .ForMember(c => c.AnswerId, opt => opt.Ignore());
            CreateMap<Question, QuestionOverview>();

            CreateMap<UserAnswerRequest, UserAnswer>();
        }
    }
}
